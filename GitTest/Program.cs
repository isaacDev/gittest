﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GitTest
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hey!!!");
            Console.WriteLine("**********************************");
            Console.WriteLine("Vamos a ver qué tal está esto del Git");
            Console.WriteLine("Indica el formato de la hora");
            Console.WriteLine("La hora exacta es: {0}", PrintDatetime(Console.ReadLine()));
            Console.ReadKey ();
            Console.Write("\n\n¿Quieres saber qué dia de la semana naciste?\n Pon tu fecha de nacimiento en el formato yyyy-dd-MM: ");
            Console.WriteLine("Tu día de nacimiento fue en: {0}", PrintDayOfBirth(Console.ReadLine()));
            Console.ReadKey();
            
        }

        public static string PrintDayOfBirth(string p)
        {
            return DateTime.Parse(p).DayOfWeek.ToString();
        }

        public static string PrintDatetime()
        {
            return DateTime.Now.ToString();
        }
        public static string PrintDatetime(string formato)
        {
            return DateTime.Now.ToString(formato);
        }

    }
}
