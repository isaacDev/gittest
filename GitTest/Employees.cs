﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace GitTest
{
    public static class Employees
    {
        public static string GetMD5_Pass(string EmployeePassword, string UserName)
        {
            MD5CryptoServiceProvider cypher = new MD5CryptoServiceProvider();
            cypher.ComputeHash(ASCIIEncoding.ASCII.GetBytes(GetSalt(UserName) + EmployeePassword));
            byte[] hash = cypher.Hash;
            StringBuilder str = new StringBuilder();
            foreach (byte byt in hash)
            {
                str.Append(byt.ToString("x2"));
            }
            return str.ToString();
        }

        private static string GetSalt(string UserName)
        {
            string salt = "";
            char[] chars = UserName.Skip(UserName.Length / 2).ToArray();
            foreach (char ch in chars)
            {
                salt += ch;
            }
            return salt;
        }
    }

}
